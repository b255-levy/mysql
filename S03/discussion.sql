-- [C]reate 

INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");

-- INSERT INTO tableName (ColumnA, culmnB, columnC ) VALUES (columnAValue, ColumnB value, ColumnC value)
INSERT INTO albums (album_title, date_release, artist_id) VALUES ("Psy 6", "2021-1-1", 2);
INSERT INTO albums (album_title, date_release, artist_id) VALUES ("Trip", "1991-1-1", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-pop", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 234, "OPM", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 230, "OPM", 2);


/*[R]etrieve*/
/*Retrieve all column values of song table*/

SELECT * FROM songs

/**/
SELECT song_name, genre FROM songs

/*Display all song_names in the sone tables and its album_id*/
-- [WHERE]

SELECT song_name from songs WHERE genre	= "OPM";
SELECT song_name, genre from songs WHERE genre	= "OPM";


/*[U]pdate*/
/*UPDATE tableName set column = value WHERE column  = value*/

UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

/* [D]elete*/
DELETE FROM songs WHERE genre = "OPM" AND length > 200;